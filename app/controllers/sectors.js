const sectors = require('../api/model/sectors_model.js');
const shortcuts = require('../api/model/shortcuts_model.js');

module.exports = {

    allSectors: function (req, res, next) {

        sectors.find()
            .then(sectorDetails => {
                res.json({
                    status: true,
                    message: "All the sector details are retrieved",
                    sectorDetails
                });
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving sectors."
                });
            });


    },

    allShortcuts: function (req, res, next) {

        shortcuts.find()
            .then(shortcutsDetails => {
                res.json({
                    status: true,
                    message: "All the shortcut details are retrieved",
                    shortcutsDetails
                });
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving shortcut."
                });
            });


    },

    createSectors: function (req, res, next) {

        sectors.create({
            name: req.body.name,
            identifier: req.body.identifier,

        }, function (err, sectorDetail) {
            if (err)
                next(err);
            else
                res.json({
                    status: true,
                    message: "The sector has been successfully added",
                    sectorDetail
                });
        })

    },

    deleteSectors: function (req, res, next) {

        sectors.findOneAndDelete({
                identifier: req.params.identifier
            })
            .then(deleteSector => {
                if (!deleteSector) {
                    return res.status(404).send({
                        message: "Sector not found with id " + req.params.identifier
                    })
                } else {
                    res.send({
                        message: "Sector deleted successfully!"
                    });
                }

            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Sector not found with id " + req.params.identifier
                    });
                }
                return res.status(500).send({
                    message: "Could not delete the sector with id " + req.params.identifier
                });
            });
    },

    deleteShortcut: function (req, res, next) {

        shortcuts.findOneAndDelete({
                _id: req.params._id
            })
            .then(deleteShortcut => {
                if (!deleteShortcut) {
                    return res.status(404).send({
                        message: "Shortcut not found with id " + req.params._id
                    })
                } else {
                    res.send({
                        message: "Shortcut deleted successfully!"
                    });
                }

            }).catch(err => {
                if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Shortcut not found with id " + req.params._id
                    });
                }
                return res.status(500).send({
                    message: "Could not delete the shortcut with id " + req.params._id
                });
            });
    },


    addShortcuts: function (req, res, next) {
        let tempShortcutArray = [];
        let shortcutArray = req.body.shortcutkey.split("")
        var counter = 0;
        for (var i = 0; i < shortcutArray.length; i++) {
           
            sectors.findOne({
                identifier: shortcutArray[i]

            }, function (err, sectorInfo) {
               
                tempShortcutArray.push({
                    sectorid: sectorInfo._id.toString(),
                    identifier: sectorInfo.identifier
                })
                counter = counter + 1;
                if (counter === shortcutArray.length) {
                    console.log(counter)
                    callback(tempShortcutArray,req.body.shortcutkey)
              
                }

                })
            }

                function callback(product,name) {

                    shortcuts.create({
                        keys: product,
                        name: name
                    }, function (err, shortcutsDetail) {
                        res.json({
                            status: true,
                            message: "Details have been successfully added",
                            shortcutsDetail
                        });
    
                    })
                }

               
       

    }
}