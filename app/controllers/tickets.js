const moment = require('moment');
const tickets = require('../api/model/tickets_model.js');
const shortcuts = require('../api/model/shortcuts_model.js');

module.exports = {

    validateTime: function (req, res, next) {

        moment().format('MMMM Do YYYY, h:mm:ss a');

        res.json({
            status: true,
            message: "Successfully Registered",
            userInfo: startDate,
        });

    },

    generateReceipt: function (req, res, next) {
        var callbackTicketId;
        var ticketId;

        tickets
            .findOne()
            .sort('-ticketid') // give me the max
            .exec(function (err, member) {
                if (err) {
                    next(err)
                } else {
                    if (member == null) {
                        ticketId = "000001"
                    } else {
                        ticketId = parseInt(member.ticketid, 10) + 1
                        if (ticketId < 10) {
                            ticketId = "00000" + ticketId
                        } else if (ticketId < 100) {
                            ticketId = "0000" + ticketId
                        } else if (ticketId < 1000) {
                            ticketId = "000" + ticketId
                        } else if (ticketId < 10000) {
                            ticketId = "00" + ticketId
                        } else if (ticketId < 100000) {
                            ticketId = "0" + ticketId
                        }


                    }
                    callback(ticketId)
                }


            });


        function callback(ticketId) {
            let allProducts = [];
            var myHTML = ''

            for (let i of req.body.numberlist) {
                let big, small, number;
                
                myHTML += i.num;

                if (i.b)    {
                    big = "B" + i.b
                    myHTML += " " + big;
                }
                else{
                    big = "B" + "0"
                    myHTML += " " + big;
                }

                if (i.s) {
                    small = "S" + i.s
                    myHTML += " " + small;
                }
                else
                {
                    small = "S" + "0"
                    myHTML += " " + small;
                }

                if (i.ibox==true) {
                    myHTML += " "+"iBOX";
                }
                

                else if(i.box == true){
                    myHTML += " " +"BOX";
                }

                else{
                    myHTML += "";
                }

                myHTML += "<br>"
            }


            callbackTicketId = ticketId
            var paymentDate = moment(req.body.pdate);


            var paymentDay = paymentDate.format('DD');
            var paymentMonth = paymentDate.format('MM');
            var paymentYear = paymentDate.format('YYYY');
            var paymentHour = paymentDate.format('HH');
            var paymentMinutes = paymentDate.format('mm');

            paymentDate = paymentDay + paymentMonth + paymentYear + " " + paymentHour + paymentMinutes;

            var currentDate = moment(req.body.cdate);

            var currentDay = currentDate.format('DD');
            var currentMonth = currentDate.format('MM');
            var currentYear = currentDate.format('YYYY');
            var currentHour = currentDate.format('HH');
            var currentMinutes = currentDate.format('mm');

            currentDate = currentDay + currentMonth + currentYear + " " + currentHour + currentMinutes;

            var identifier = "#" + req.body.identifier;


            tickets.create({
                userid: req.body.userid,
                ticketid: ticketId,
                currentdate: currentDate,
                paymentdate: paymentDate,
                identifier: identifier,
                total: req.body.total,
                numberlist: req.body.numberlist,

            }, function (err, result) {
                if (err)
                    next(err);
                else
                
                    res.json({
                        data: "<html><head></head><body><h2><strong>" + currentDate + "<br>"+"</strong></h2><h2><strong>" + ticketId + "<br>"+"</strong></h2><h2><strong>" + req.body.userid + "<br>"+"</strong></h2><h2><strong>" + identifier + "<br>" +
                        "</strong></h2><h2><strong>" + myHTML + "GT"+req.body.total + "</strong></h2></body></html>"
                    })
                res.end();
            })

        }
    }
}