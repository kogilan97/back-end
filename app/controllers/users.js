const users = require('../api/model/users_model.js');
const tokenconfig = require('../../config/token.js');
const jwt = require('jsonwebtoken');

const bcrypt = require('bcryptjs');

module.exports = {

    login: function (req, res, next) {

        users.findOne({
            userid: req.body.userid,
            username: req.body.username

        }, function (err, userInfo) {

            if (err) {
                next(err);
            } else {
                if (userInfo == null) {
                    return res.status(200).send({
                        status: false,
                        message: 'Incorrect User ID or Username'
                    });
                } else {
                    bcrypt.compare(req.body.password, userInfo.password, function (err, result) {
                        if (result == true) {
                            const token = jwt.sign({
                                    userid: userInfo.userid
                                },
                                tokenconfig.secret
                            );

                            return res.status(200).send({
                                status: true,
                                message: 'You have successfully logged into the system',
                                token: token,
                                userInfo
                            });
                        } else {
                            return res.status(200).send({
                                status: false,
                                message: 'Incorrect Password'
                            });
                        }
                    })

                }
            }
        })
    },

    createUser: function (req, res, next) {

        bcrypt.hash(req.body.password, 10, function (err, hash) {
            users.create({
                userid: req.body.userid,
                username: req.body.username,
                status: "Active",
                role: "Agent",
                password: hash,
            }, function (err, result) {
                if (err)
                    next(err);
                else
                    res.json({
                        status: true,
                        message: "Successfully Registered",
                        userInfo: result,
                    });
            })
        })
    },

    generateUserID: function (req, res, next) {
        let newValue;
        users
            .findOne()
            .sort('-userid') // give me the max
            .exec(function (err, member) {

                if (err) {
                    next(err)
                } else {
                    if (member == null) {
                        newValue = "A001"
                    } else {
                        newValue = parseInt(member.userid, 10) + 1
                        if (newValue < 10) {
                            newValue = "A00" + newValue
                        } else if (newValue < 100) {
                            newValue = "A0" + newValue
                        }
                    }
                }
                    res.json({
                    status: true,
                    message: "Id has been retrieved",
                    userid: newValue,
                });

            });
    }

}