const mongoose = require('mongoose');

const sectorSchema = mongoose.Schema({
        identifier: {
            type: String,
            unique: true
        },
        name: String
    },

)
module.exports = mongoose.model('sectors', sectorSchema);