const mongoose = require('mongoose');

const ticketSchema = mongoose.Schema({
    ticketid:String,
    currentdate:String,
    paymentdate:String,
    userid: String,
    total: String,
    identifier: String,
    numberlist : [{
        num: String,
        b: String,
        s:String,
        ibox:Boolean,
        box:Boolean
    }]

    }
)
module.exports = mongoose.model('tickets', ticketSchema);