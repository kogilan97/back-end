const mongoose = require('mongoose');               

const UserSchema = mongoose.Schema ({
userid: {
    type:String,
    unique:true
},
password: String,
username: String,
role: String,
status:String
},

)
module.exports=mongoose.model('users', UserSchema);

