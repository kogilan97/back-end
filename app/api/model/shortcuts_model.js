const mongoose = require('mongoose');

const shortcutSchema = mongoose.Schema({
    keys : [{
        sectorid: String,
        identifier: String
    }],
    name: String
    }
)
module.exports = mongoose.model('shortcuts', shortcutSchema);