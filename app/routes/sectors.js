const express = require('express');
const sectors = require('../controllers/sectors.js');

const router = express.Router();

router.get('/all', sectors.allSectors);
router.get('/allShortcuts', sectors.allShortcuts);
router.post('/create', sectors.createSectors);
router.delete('/delete/:identifier', sectors.deleteSectors);
router.delete('/delete/shortcut/:_id', sectors.deleteShortcut);
router.post('/shortcut/add', sectors.addShortcuts);

module.exports = router;