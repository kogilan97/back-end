const express = require('express');
const tickets = require('../controllers/tickets.js');


const router = express.Router();

router.post('/validatetime', tickets.validateTime);
router.post('/receipt', tickets.generateReceipt);

module.exports = router;