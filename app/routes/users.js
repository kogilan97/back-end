const express = require('express');
const users = require('../controllers/users.js');

const router = express.Router();

//Create a new User
router.post('/register', users.createUser);

//Login 
router.post('/login', users.login);

//Generate Token
router.get('/id', users.generateUserID);

module.exports = router;