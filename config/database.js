const mongoose1 = require('mongoose');                                                                                  
const mongoDB = 'mongodb+srv://admin:123@calculatetrack.x2wwi.mongodb.net/track?retryWrites=true&w=majority';

mongoose1.connect(mongoDB,{ useNewUrlParser: true, useFindAndModify: false, useCreateIndex:true, useUnifiedTopology:true }).then(()=>
{
console.log("Successfully connected to the database");
}).catch(err => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
});
mongoose1.Promise = global.Promise;

module.exports = mongoose1;